package com.employeeManagement.employee.repository;

import com.employeeManagement.employee.model.Employee;
import org.springframework.data.jpa.repository.JpaRepository;



public interface EmployeeRepository extends JpaRepository<Employee, Long> {
}
