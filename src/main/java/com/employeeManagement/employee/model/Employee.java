package com.employeeManagement.employee.model;

import jakarta.persistence.*;
import lombok.Data;
@Data
@Entity//It will create an jpa entity
@Table(name="employees")//It will create table in the database
public class Employee {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @Column(name="first_name",nullable=false)
    private String firstname;
    @Column(name="last_name")
    private String lastname;
    @Column(name="email")
    private String email;
}
