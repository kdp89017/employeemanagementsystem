package com.employeeManagement.employee.service.impl;

import com.employeeManagement.employee.exception.ResourceNotFoundException;
import com.employeeManagement.employee.model.Employee;
import com.employeeManagement.employee.repository.EmployeeRepository;
import com.employeeManagement.employee.service.EmployeeService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class EmployeeServiceImpl implements EmployeeService {


    private EmployeeRepository employeeRepository;

    public EmployeeServiceImpl(EmployeeRepository employeeRepository) {
        super();
        this.employeeRepository = employeeRepository;
    }

    @Override
    public Employee saveEmployee(Employee employee) {
        return employeeRepository.save(employee);
    }

    @Override
    public List<Employee> getAllEmployees() {
        return employeeRepository.findAll();
    }

    @Override
    public Employee getEmployeeById(long id) {
       /* Optional employee= employeeRepository.findById(id);
        if(employee.isPresent()) {
            return (Employee)employee.get();
        }else {
            throw new ResourceNotFoundException("Employee","Id",id);
        }
        */
        return employeeRepository.findById(id).orElseThrow(()->new ResourceNotFoundException("Employee","id",id));
    }

    @Override
    public Employee updateEmployee(Employee employee, long id) {
        //we need to chech whether employee with given is is exist in DB or not
        Employee existingEmployee=  employeeRepository.findById(id).orElseThrow(
                ()->new ResourceNotFoundException("Employee","id",id));
        existingEmployee.setFirstname(employee.getFirstname());
        existingEmployee.setLastname(employee.getLastname());
        existingEmployee.setId(employee.getId());
        existingEmployee.setEmail(employee.getEmail());
        //save existing employee to DN
        employeeRepository.save(existingEmployee);
        return existingEmployee;
    }

    @Override
    public void deleteEmployee(long id){
        //check whether a employee exist in a DB or not
        employeeRepository.findById(id).orElseThrow(()->new ResourceNotFoundException("Employee","id",id));
        employeeRepository.deleteById(id);
    }

}
