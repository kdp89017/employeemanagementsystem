package com.employeeManagement.employee.controller;

import com.employeeManagement.employee.model.Employee;
import com.employeeManagement.employee.service.EmployeeService;
import jakarta.annotation.PostConstruct;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/employees")
public class EmployeeController {
    private EmployeeService employeeService;

    public EmployeeController(EmployeeService employeeService) {
        this.employeeService = employeeService;
    }
    //build create employee REST API
    //by using return type as ResponseEntity we can provide complete details in this class
   @PostMapping("/addemployee")
    public ResponseEntity<Employee> saveEmployee(@RequestBody Employee employee) {

        return new ResponseEntity<Employee>(employeeService.saveEmployee(employee), HttpStatus.CREATED);
    }
    //Build get all employees Rest API
    @GetMapping("/getallemployees")
    public List<Employee> getAllEmployees() {
        return employeeService.getAllEmployees();
    }
    //build get employee by id rest api
    //http://localhost:8080/api/employees/getemployee/1
    @GetMapping("/getemployee/{id}")
    public ResponseEntity<Employee>getEmployeeById(@PathVariable(name="id") long id){
        return new ResponseEntity<Employee>(employeeService.getEmployeeById(id), HttpStatus.OK);
    }

    //build update employee Rest API
    //http://localhost:8080/api/employees/updateemployee/1
    @PutMapping("/{id}")
    public ResponseEntity<Employee> updateEmployee(@RequestBody Employee employee ,@PathVariable("id") long id){
        return new ResponseEntity<Employee>(employeeService.updateEmployee(employee,id),HttpStatus.OK);
    }

    //Build delete employee Rest API
    //http://localhost:8080/api/employees/updateemployee/1
    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteEmployee(@PathVariable("id") long id){
        employeeService.deleteEmployee(id);
        return new ResponseEntity<String>("Employee Deleted successfully", HttpStatus.OK);
    }
}
